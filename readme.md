# Installtionsanleitung f�r Miktex:

1. Beliebigen eigenen Ordner erstellen
2. Unterordner tex erstellen
3. Unterordner latex erstellen
4. Struktur sollte so aussehen: ../"Eigener Ordner"/tex/latex/
5. physmak.sty in den Ordner latex kopieren
6. Miktex-Settings (Admin) �ffnen
7. Im Tab "Roots" den eigenen Ordner ../"Eigener Ordner"/ als Path hinzuf�gen.
8. Im Tab "General" "Refresh FNDB" durchf�hren.
9. Fertig.

# Installationsanleitung f�r OS X/Texlive:

    git clone git@bitbucket.org:8b533/physmak.git
    cd physmak
    cp -vrf tex ~/Library/texmf/tex

# Beinhaltet folgende Makros:

    \ve [1] : \ensuremath{\vec{#1}}
    \op [1] : \ensuremath{\widehat{#1}}
	\tensor [1] : \overleftrightarrow{#1}
    \ket [1] : \ensuremath{\lvert \, #1 \, \rangle}
    \bra [1] : \ensuremath{\langle \, #1 \, \rvert}
    \innerp [2] : \ensuremath{\langle \, #1 \, | \, #2 \, \rangle}
    \outerp [2] : \ensuremath{\ket{#1} \bra{#2}}
    \expv [3] : \ensuremath{\langle \, #1 \, | \, #2 \, | \, #3 \, \rangle}
    \abs [1] : \ensuremath{\lvert #1 \rvert}
    \floor [1] : \ensuremath{\lfloor #1 \rfloor}
    \ceil [1] : \ensuremath{\lceil #1 \rceil}
    \pdiff [2] : \ensuremath{\frac{\partial #1}{\partial #2}}
    \diff [2] : \ensuremath{\frac{d #1}{d #2}}
    \mean [1] : \ensuremath{\langle \, #1 \, \rangle}
    \setN : \ensuremath{\mathbb{N}}
    \setR : \ensuremath{\mathbb{R}}
    \setC : \ensuremath{\mathbb{C}}
    \setZ : \ensuremath{\mathbb{Z}}
    \cmpl [1] : \ensuremath{\overline{#1}}
    \pois [2] : \ensuremath{\bigl\{ #1, #2 \bigr\}}
    \comm [2] : \ensuremath{\bigl[ #1, #2 \bigr]}
    \rhoBar : \ensuremath{\overline{\rho}}
    \oneop : \ensuremath{\op{\mathds{1}}}
	\onemat : \ensuremath{\mathds{1}}
    \cmcal : {OMS}{cmsy}{m}{n}
    \event : \ensuremath{\cmcal{E}}
    \events : \ensuremath{\cmcal{S}}
    \eventAl : \ensuremath{\cmcal{A}}
    \asop : \ensuremath{\hat{\cmcal{A}}}
    \sop : \ensuremath{\hat{\cmcal{S}}}
    \impevent : \ensuremath{\cmcal{O}}
    \subevents : \ensuremath{\cmcal{M}}
	\reffig [1] : Abbildung~\ref{#1}
	\refeq [1] : Gleichung~(\ref{#1})
	\reftab [1] : Tabelle~\ref{#1}
	\refsec [1] : Abschnitt~\ref{#1}
	\aCreationOp : \ensuremath{\op{a}^\dagger}
	\aAnnihilOp : \ensuremath{\op{a}}
	\sCreationOp : \ensuremath{\op{b}^\dagger}
	\sAnnihilOp : \ensuremath{\op{b}}
	\integral [1] : \ensuremath{\int \mathrm{d} #1}
	\pint [1] : \ensuremath{\int \mathcal{D} #1}
    
## MathOperators    
    \hilbert : \ensuremath{\cmcal{H}}
    \Pot : \cmcal{P}
    \Div : div
    \Tr : Tr
	\Rot : rot
	\Grad : grad
	\sgn : sgn

## Optionen
	bv : fettgedruckte Vektoren
	boldtensor : Tensoren werden fettgedruckt
	tildetensor : Tensoren werden mit einer Tilde gekennzeichnet
	english: Referenzen werden in Englisch geschrieben
	smallop: operators werden mit einem kleinen Operatorzeichen beschriftet